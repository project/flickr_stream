Flickr Stream
==============

Project site: <https://www.drupal.org/project/flickr_stream>

Issues: <https://drupal.org/project/issues/flickr_stream>

What Is This?
-------------

This module allows users to embed media stream from Flickr.com.
It provides the user with the preset number of the latest images from specified Flickr accounts

How To Install The Modules
--------------------------

1.The project requires ImageCache External module to be installed first.

2.The project installs like any other Drupal module.

There is extensive documentation on how to do this here:
<https://drupal.org/documentation/install/modules-themes/modules-8>
But essentially: Download the tarball and expand it into the modules/ directory
in your Drupal 8 installation.

3.Within Drupal, enable Flickr Stream module

Setup Flickr Stream module
--------------------------

4.Setup your Flickr Service App here
to get Flickr credentials as key and secret
<https://www.flickr.com/services/apps/create/>
and copy them here /admin/config/media/flickr_stream

4.Features

- new flickr_stream field type
- new block 'Flickr stream'
- ability to define number of photos from the specified Flickr accounts (stream) with or without Albums specification
- ability to specify a different Flickr account for each node